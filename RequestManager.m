///  RequestManager.m
//  Runtime
//
//  Created by Anil Can Baykal on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RequestManager.h"
#import "HandsomeManager.h"
#import "HandsomeUtil.h"


//==============================================================================
@interface RequestManager(Private)



- (void)requestWentGood:(HandsomeRequest*) req;
//- (void)requestDone:(AFHTTPRequestOperation*)request;
//- (void)requestWentWrong:(AFHTTPRequestOperation*)request;

-(void)reportFailedforRequest:(HandsomeRequest*)req;

@end



//==============================================================================

@implementation RequestManager

static RequestManager *sharedInstance = nil;



+ (id)sharedManager
{
    static RequestManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RequestManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}


//==============================================================================
- (id)init
{
    self = [super init];
    
    if (self) {
        
        self.session = [HandsomeUtil generateUuidString];
        registerId = [[NSUserDefaults standardUserDefaults] objectForKey:registerIdKey];
               
        
        // Initialization code here.
        queue = [[NSOperationQueue alloc] init];
        [queue setMaxConcurrentOperationCount:1];
        
        //self.userAgent = [[NSUserDefaults standardUserDefaults] objectForKey:userAgentKey];
        //if ( self.userAgent == nil)
        [self discoverUserAgent];
        
        requestCache = [NSMutableDictionary new];        
    }
    
    return self;
}

- (void)dealloc {
    
    queue = nil;
    requestCache = nil;
}

//==============================================================================
// steal user agent from a dummy webview -
-(void)discoverUserAgent{
    /*
    UIWebView * _kolpa = [[UIWebView alloc] initWithFrame:CGRectZero];
    _kolpa.delegate = self;
    [_kolpa loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://whatsmyuseragent.com"]]];
    */
    if (!self.userAgent) {
        UIWebView *webview = [[UIWebView alloc] init];
        self.userAgent = [[webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"] copy];
    }
}

//==============================================================================
-(NSString*)globalSession {    
    return self.session;
}


-(void) applicationRegistered:(NSString*)regId{
    
    [[NSUserDefaults standardUserDefaults] setObject:regId forKey:registerIdKey];
    [[NSUserDefaults standardUserDefaults] synchronize]; 
    registerId = regId;
}


//==============================================================================
// calculate a fingerprint signature from dat
-(NSString *)getSignature:(NSData*) data {
    
    unsigned char digest[CC_SHA256_DIGEST_LENGTH];
    CC_LONG len = data.length;
    CC_SHA256(data.bytes, len, digest);
    
    // 11 12 14 18 	RANDOM
    // 13 17 19 25 	RANDOM
    static int set1[4] = {11, 12, 14, 18}; 
    static int set2[4] = {13, 17, 19, 25}; 
    /* ad set to increase security.... */    
    
    int * set = ((arc4random() % 2) == 0)? set1 : set2;    

    // TODO
    //NSMutableString* hash = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    //for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
    //[hash appendFormat:@" %d.%02x",i, digest[i]];
        
    
    NSMutableString* signature = [NSMutableString stringWithCapacity:SIGNATURE_LENGTH * 2];    
    for(int i = 0; i < SIGNATURE_LENGTH-1; i++)
        [signature appendFormat:@"%02x", digest[set[i]]];
            
    // RANDOM
    [signature appendFormat:@"%02x", (arc4random() % 29) + 'A'];
    
    //_NSLog(@"\nhash %@\nsignature %@", hash, signature);
    return signature;     
}


//==============================================================================
- (void) addRequest:(HandsomeRequest*) req {
	// must be present on run time;
    req.session 	= self.session;
        
    req.startTime = [NSDate timeIntervalSinceReferenceDate];
    req.deviceId = [[HandsomeManager shared] deviceId]; 
    
    // target address
    NSURL * host = [req getHost]; 
    _ASSERT(nil!= host);
    NSURL * url = [host URLByAppendingPathComponent:req.verb]; 
	
	
    NSString * handsome_time = [NSString stringWithFormat:@"%.0f", [NSDate timeIntervalSinceReferenceDate]];
    
	NSString * key 		= @"request";         
	NSDictionary * data = [NSObject serialize:req];
     	
	NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:data, key,  nil]; 
	//NSData * output = [[CJSONSerializer serializer] serializeDictionary:dict error:&erol];
    //NSData * output = [dict JSONData];
    NSData * output = [NSJSONSerialization dataWithJSONObject:dict
                                                      options:NSJSONWritingPrettyPrinted
                                                        error:nil];

	_NSLog(@"\n//==============================================================================\n%@ -> \n|%@|\n",url,
          [NSString stringWithCString:output.bytes encoding:NSUTF8StringEncoding]);


    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest addValue:@"application/json; charset=utf-8"
      forHTTPHeaderField:@"Content-Type"];
    
    [urlRequest addValue:handsome_time
      forHTTPHeaderField:HANDSOME_HEADER_TIME];
    
    [urlRequest addValue:[[HandsomeManager shared] deviceId]
      forHTTPHeaderField:HANDSOME_HEADER_DEVICEID];
    
    [urlRequest addValue:[[HandsomeManager shared] applicationToken]
      forHTTPHeaderField:HANDSOME_HEADER_TOKEN];
        
    [urlRequest addValue:@"iOS"
      forHTTPHeaderField:HANDSOME_HEADER_SOURCE];

#ifdef VERSION
    
    // IF  YOU WANT TO UPDATE VERSION GOTO **build.sh**
    NSString * VERSION_STRING = @"" MACRO_VALUE_TO_STRING(VERSION) "";
    [urlRequest addValue:VERSION_STRING
      forHTTPHeaderField:HANDSOME_HEADER_VERSION];
#endif
    
        
#ifdef TOKEN
    // token to hash
    NSString * tokenSource = [NSString stringWithFormat:@"%@%@%@",handsome_time,req.session,req.verb];
    
    [urlRequest addValue:tokenSource
      forHTTPHeaderField:@"x-handsome-tokensource"];
#endif
        
    //if (self.userAgent)
    [urlRequest addValue:self.userAgent forHTTPHeaderField:@"User-Agent"];
    
#ifdef DEBUG
    [urlRequest setTimeoutInterval:75];
#else
    [urlRequest setTimeoutInterval:45];
#endif

    [urlRequest setHTTPBody:output];

    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse * response, NSData* data, NSError*error){
        _NSLog(@"<HANDSOME:%@>:response received", response.URL.path.lastPathComponent);
        if ( response != nil){
            // server response
            req.statusCode = [(NSHTTPURLResponse *)response statusCode];
        } else if ( error != nil){
            // CFNetworkErrors
            req.statusCode = error.code;
        }
        
        [self requestDone:req response:(NSHTTPURLResponse*)response data:data];
        
//        if ( error == nil )
//            [self requestDone:req response:(NSHTTPURLResponse*)response data:data];
//        else
//            [self requestWentWrong:req response:(NSHTTPURLResponse*)response error:error];
        
    }];
    
    //callback 
    if ([req.delegate respondsToSelector:@selector(requestWillStart:)])
        [req.delegate performSelector:@selector(requestWillStart:) 
                           withObject:req];
}

//==============================================================================
-(void)requestDone:(HandsomeRequest*)req response:(NSHTTPURLResponse*)response data:(NSData*)input{
    
    req.requestDuration = [NSDate timeIntervalSinceReferenceDate] - req.startTime;
    
    req.response = [self readResponse:input];
    if( req.response == nil ||  [req.response isKindOfClass:[InnerFault class]]){
        [self reportFailedforRequest:req];
    } else {
        [self requestWentGood:req];
    }

}

-(HandsomeResponse*)readResponse:(NSData*)input{
   
    // deserialize jsonstring
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:input
                                                          options:0
                                                            error:nil];
    
    HandsomeResponse * res = nil;
    NSString * resultKey 	= @"Result";
    NSString * errorKey 	= @"Error";
    
    NSDictionary * result = [dict objectForKey:resultKey];
    if ( result != nil)
    	res = [NSObject deserialize:result];
    else if ( [dict objectForKey:errorKey]) {
        res = [NSObject deserialize:[dict objectForKey:errorKey]];
        
    }
    
    return res;
    
}

//==============================================================================
// called when nsurlconnection is successfuly finished without failure
-(void)requestWentGood:(HandsomeRequest*) req{
    
    __block HandsomeRequest *_req = req;
    dispatch_async(dispatch_get_main_queue(), ^(){
    
        if ( req.successBlock != nil) {
            
            req.successBlock(_req);
        }
        
        if ([req.delegate respondsToSelector:@selector(requestFinished:withResponse:)]) {
            [req.delegate performSelector:@selector(requestFinished:withResponse:)
                               withObject:req
                           withObject:req.response];
        }
        
    });
}



//==============================================================================
// called when http request is not 200 or HandsomeResult  is not OK
-(void)reportFailedforRequest:(HandsomeRequest*)req{
    
    __block HandsomeRequest *_req = req;
    dispatch_async(dispatch_get_main_queue(), ^(){

        if (req.failureBlock != nil)
            req.failureBlock(_req);
        
        if ([req.delegate respondsToSelector:@selector(requestFailed:withError:)])
            [req.delegate performSelector:@selector(requestFailed:withError:)
                               withObject:req
                               withObject:nil];

        
    });
}

@end

//
//  HandsomeManager.m
//  HandsomeLibrary
//
//  Created by Anil Can Baykal on 11/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HandsomeManager.h"
#import "HandsomeUtil.h"
#import "Debug.h"
#import "RequestManager.h"

//#import "NSString+MD5Addition.h"
#import <AdSupport/AdSupport.h>

static HandsomeManager* _sharedInstance = nil;




//==============================================================================

@interface HandsomeManager()

/* EVENT DISPATCHER */
@property (nonatomic, retain) NSMutableArray * 	eventItems;
@property (nonatomic, retain) NSTimer * 		eventTimer;
@property (nonatomic) float eventDispatchTime;
@property (nonatomic, retain) NSDateFormatter * eventItemTimeStamp;

/* TRANSACTION */
@property (nonatomic, retain) NSMutableArray * transactions;

@property (nonatomic, retain) NSDateFormatter * timeFormatter;
@property (nonatomic, retain) NSDateFormatter * dateFormatter;


@property (nonatomic, copy)NSString *applicationToken;
@property (nonatomic, copy)NSString *deviceId;
@property (nonatomic, retain) HandsomeUser * currentUser;



@end


//==============================================================================
@implementation HandsomeManager

@synthesize tracking;
@synthesize dispatchTime;
@synthesize openUdId;

+(void)initialize {
    
    static BOOL initialized = NO; 
    if ( !initialized){
        initialized = YES; 
        _sharedInstance = [HandsomeManager new];
        
        
        //[yourArray writeToFile:yourArrayFileName atomically:YES];
    }
}

+(HandsomeManager*)shared{
    return _sharedInstance; 
}

//==============================================================================
- (id)init{
    
    self = [super init];
    if (self) {
        
        self.deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:deviceIdKey];
        
        if ( self.deviceId == nil){
            //self.deviceId = [HandsomeUtil generateUuidString] ;
            //self.deviceId = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
            self.deviceId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
            [[NSUserDefaults standardUserDefaults] setObject:self.deviceId forKey:deviceIdKey];
        }
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(applicationWillTerminate:)
                                                    name:UIApplicationWillTerminateNotification
                                                  object:[UIApplication sharedApplication]];
        
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(applicationWillBecomeActive:)
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:[UIApplication sharedApplication]];
        
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(applicationWillTerminate:)
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:[UIApplication sharedApplication]];
        
        
        
        self.tracking = NO;
        
    }
    return self;
}

- (void)dealloc{
    
    self.eventItems = nil;
    self.eventTimer = nil; 

}

//notification callbacsk
-(void)applicationWillTerminate:(NSNotification*)notification{
    
}

-(void)applicationWillBecomeActive:(NSNotification*)notification{

    
}


-(void)setProjectId:(NSString *)projectId{
    self.applicationToken = projectId;
}



//==============================================================================
#pragma mark MDATE
-(MDate*)mdateFromDate:(NSDate*)date{
    
    MDate * result = [MDate new];
    result.time = [self.timeFormatter stringFromDate:date];
    result.date = [self.dateFormatter stringFromDate:date];
    
    return result;
}

-(NSDate*)dateFromMdate:(MDate*)mdate{
    
    NSString * dateString = [NSString stringWithFormat:@"%@ %@%@",
                             mdate.time, mdate.date, mdate.zone];
    
    
    NSString *dateFormat = @"HH:mm:ss dd/MM/yyyyZ";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate* date = [dateFormatter dateFromString:dateString];
    return date;
}

-(MDate*)now{
    return [self mdateFromDate:[NSDate date]];
}

//==============================================================================
#pragma mark - LAUNCH
-(void)launch:(NSString *)applicationToken{
    [self launch:applicationToken trackSession:YES];
}

-(void)launch:(NSString *)applicationToken trackSession:(BOOL)_trackSession{
    self.tracking= _trackSession;
    [self launch:applicationToken withDispatchTime:kDefaultEventDispatchTime];
}

-(void)launch:(NSString *)applicationToken withDispatchTime:(int)eventDispatchTime{
    

}


-(NSString *)deviceID{
    return _deviceId;
}

-(NSString *)applicationToken{
    return _applicationToken;
}



//==============================================================================
// USER MANAGEMENT
#pragma mark - USER MANAGEMENT
-(HandsomeUser*)currentUser{
    return _currentUser;
}

-(NSString *)hashPassword:(NSString *)password{
    //return [password stringFromMD5];
    return password;
}


@end


//==============================================================================
#pragma mark - HANDSOME OBJECT
@implementation HandsomeObject (DataStore)
/*
-(void)save{
    [self saveWithTarget:nil andSelector:nil];
}

-(void)saveWithCallback:(HandsomeCallback)callback{
    [self saveWithTarget:nil andSelector:nil];
}


-(void)saveWithTarget:(id)target andSelector:(SEL)selector{
    
    
    SaveRequest * request = [[SaveRequest alloc] initWithDelegate:_sharedInstance];
    request.applicationToken = [[HandsomeManager shared] applicationToken];
    NSData * data = [NSJSONSerialization dataWithJSONObject:[NSObject serialize:self] options:NSJSONWritingPrettyPrinted error:nil];
    request.serializedObject =  [NSString stringWithCString:data.bytes encoding:NSUTF8StringEncoding];
    //request.objectKey = self.objectKey;
    
    request.successBlock = ^(){
        NSLog(@"saved succesfully");
        SaveResponse * response = (SaveResponse*)request.response;
        
        if ([target respondsToSelector:selector])
            [target performSelector:selector withObject:self];
        
        if ( objectKey != nil){
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationHandsomeObjectUpdated object:self];
        } else {
            objectKey = response.objectKey;
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationHandsomeObjectSaved object:self];
        }
        
    };
    
    request.failureBlock = ^(){
        NSLog(@"failed to save");
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationHandsomeObjectFailed object:self];
    };
    
    [request run];        
}

////////////
// DELETE 

-(void)remove{
    [self removeWithTarget:nil andSelector:nil];
}

-(void)removeWithTarget:(id)target andSelector:(SEL)selector{
    
    DeleteRequest * request = [[[DeleteRequest alloc] initWithDelegate:_sharedInstance] autorelease];
    request.objectKey = objectKey;
    request.applicationToken = [[HandsomeManager shared] applicationToken];
    request.successBlock = ^(){
        _NSLog(@"removed succesfully");
        if ([target respondsToSelector:selector])
            [target performSelector:selector withObject:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationHandsomeObjectDeleted object:self];
        
    };
    
    request.failureBlock = ^(){
        _NSLog(@"failed to remove");
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationHandsomeObjectFailed object:self];
    };
    
    [request run];
}

///////////
// LIST 
//+(void)listHandsomeObjects:(NSString *)className target:(id)target selector:(SEL)selector{
+(void)listHandsomeObjects:(NSString *)className complete:(HandsomeUserQueryCallBack)callback{
    
    ListRequest * request = [[[ListRequest alloc] initWithDelegate:nil] autorelease];
    
    // FIXME
    //request.objectType = className;
    QueryFilter * typeFilter = [[QueryFilter alloc] init];
    typeFilter.fieldName = typeIdentifier;
    typeFilter.fieldValue = className;
    request.filters = [NSMutableArray arrayWithArray:@[typeFilter]];
    
    request.applicationToken = [[HandsomeManager shared] applicationToken];
    request.successBlock = ^(){
        NSLog(@"list succesfully");
        ListResponse * response = (ListResponse*)request.response;
        //if ([target respondsToSelector:selector])
        //    [target performSelector:selector withObject:response.results];
        
        NSMutableArray * resultsParsed = [NSMutableArray arrayWithCapacity:response.results.count];
        for (NSString *raw in response.results){
            NSData * data = [raw dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            HandsomeObject * obj = [NSObject deserialize:dict];
            [resultsParsed addObject:obj]; 
        }
        callback(resultsParsed);
    };
    
    request.failureBlock = ^(){
        NSLog(@"failed to list");
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationHandsomeObjectFailed object:self];
    };
    
    [request run];
    
}

@end

@implementation HandsomeUser(UserInfoAddition)

-(void)setObject:(id)object name:(NSString *)name{
    
    AddUserInfoRequest * request = [[[AddUserInfoRequest alloc] initWithDelegate:_sharedInstance] autorelease];
    request.applicationToken = _sharedInstance.applicationToken; // this is stupid
    request.info.ownerKey = self.objectKey;
    request.info.name = name;
    
    NSDictionary * valueDict = nil;
    
    if ( [object isMemberOfClass:[HandsomeObject class]]){        
        valueDict = [NSObject serialize:object]; 
    } else {
        valueDict = @{@"_value":object};
    }
    
    NSData * rawData 	= [NSJSONSerialization dataWithJSONObject:valueDict options:0 error:nil];
    request.info.value 	= [NSString stringWithCString:rawData.bytes encoding:NSUTF8StringEncoding];
    
    request.successBlock = ^(){
        //AddUserInfoRequest * response = (AddUserInfoResponse *)request.response;
        //_NSLog(@"%@",response);
    };
        
    [request run];
}

-(void)getObject:(NSString *)name completion:(HandsomeUserInfoCallBack)callback{

    GetUserInfoRequest * request = [[[GetUserInfoRequest alloc] initWithDelegate:_sharedInstance] autorelease];
    request.applicationToken = _sharedInstance.applicationToken; // this is stupid
    request.ownerKey = self.objectKey;
    request.name = name;
    
    request.successBlock = ^(){
        
        GetUserInfoResponse * response = (GetUserInfoResponse *)request.response;
        _NSLog(@"%@",response);
        UserInfo * info = response.userInfo;
        id result = nil; 
        
        NSData * data = [info.value dataUsingEncoding:NSUTF8StringEncoding];
        
        if(data== nil) {
            callback(nil);
            return ;
        }
        
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        if ( [dict objectForKey:typeIdentifier])
            result = [NSObject deserialize:dict];
        else
            result = [dict objectForKey:@"_value"];
        
        callback([result retain]);
    };
    
    
    [request run];
}
*/


@end
